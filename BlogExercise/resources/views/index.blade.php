@extends('master')

@section('content')
    <header class="container-fluid">
        <div class="row">
            <div class="col hero text-white text-center d-flex align-items-center justify-content-center flex-column">
                <h1 class="title">Brainster.xyz Labs</h1>
                <p class="p-2">Проекти од академиите на Brainster</p>
            </div>
        </div>
    </header>

<main class="container p-5">
    <div class="row grid-container faded-cards hide">

        @foreach ($products as $product)
            <div class="card d-flex h-100 flex-column justify-content-between" id="{{ $product->id }}">
                <a href="{{ $product->url }}" target="_blank">
                    <img class="card-img p-2" src="{{ $product->photo }}" alt="Card image cap">
                    <div class="card-title text-secondary m-0">
                        <h5 class="card-title">{{ $product->name }}</h5>
                        <p class="card-text">{{ $product->subtitle }}</p>
                    </div>
                    <div class="card-body">
                        <p class="card-text">{{ $product->description }}</p>
                    </div>
                </a>
            </div>  
        @endforeach  

    </div>
</main>
@endsection