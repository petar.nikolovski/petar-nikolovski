@extends('master')

@section('content')
<main class="container py-3" style="height: 75vh">
    <div class="row">
        <form class="col-lg-6 col-md-8 col-sm-11 mx-auto" method="POST" action="{{ route('authenticate') }}">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @if(session()->has('error'))
                <div class="alert alert-danger">
                    {{ session()->get('error') }}
                </div>
            @endif

            <div class="form-group" style="margin-top: 20vh">
                <label for="email">Е-мејл</label>
                <input type="email" class="form-control" id="email" name="email" value="">
            </div>
            <div class="form-group">
                <label for="password">Пасворд</label>
                <input type="password" class="form-control" id="password" name="password" value="">
            </div>
            <button type="submit" class="btn btn-light btn-block">Логирај се</button>
            @csrf
        </form>
    </div>
</main>
@endsection