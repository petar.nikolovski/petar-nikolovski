@extends('master')

@section('content')
<main class="container py-3">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="add-new-tab" role="tab" data-toggle="tab" aria-controls="add-new-tab" aria-selected="true"
                href="#add-new">Додај</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="edit-tab" role="tab" data-toggle="tab" aria-controls="edit-tab" aria-selected="false" href="#edit">Измени</a>
        </li>
    </ul>

    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" role="tabpanel" id="add-new">
            <h4 class="py-3">Додај нов производ:</h4>
            <div class="col-12"></div>

            <form class="col-8 mx-auto" method="POST" action=" {{ route('add') }} ">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif

                <div class="form-group">
                    <label for="name">Име</label>
                    <input class="form-control" type="text" id="name" name="name" value="" />
                </div>
                <div class="form-group">
                    <label for="subtitle">Поднаслов</label>
                    <input class="form-control" type="text" id="subtitle" name="subtitle" value="" />
                </div>
                <div class="form-group">
                    <label for="photo">Слика</label>
                    <input class="form-control" type="url" id="photo" name="photo" placeholder="http://" value="" />
                </div>
                <div class="form-group">
                    <label for="url">URL</label>
                    <input class="form-control" type="url" id="url" name="url" placeholder="http://" value="" />
                </div>
                <div class="form-group">
                    <label for="description">Опис</label>
                    <textarea class="form-control" id="description" name="description" value="" ></textarea>
                </div>

                <button type="submit" class="btn btn-light btn-block">Додај</button>
                @csrf
            </form>
        </div>

        <div class="tab-pane fade" role="tabpanel" id="edit">
            <h4 class="py-3">Измени постоечки производи:</h4>

            <div class="row grid-container">

                @foreach ($products as $product)
                    <div class="card d-flex h-100 flex-column justify-content-between" id="{{ $product->id }}">
                        <a href="{{ $product->url }}" target="_blank">
                            <img class="card-img p-2" src="{{ $product->photo }}" alt="Card image cap">
                            <div class="card-title text-secondary m-0">
                                <h5 class="card-title">{{ $product->name }}</h5>
                                <p class="card-text">{{ $product->subtitle }}</p>
                            </div>
                            <div class="card-body">
                                <p class="card-text">{{ $product->description }}</p>
                            </div>
                        </a>
                        <div class="card-footer">
                            <div class="controls">
                                <button class="edit-button edit-{{ $product->id }} fas fa-pen-square fa-2x"></button> 
                                <button class="fas fa-times fa-2x delete-button" data-toggle="modal" data-target="#deleteModal"></button>
                            </div>
                        </div>
                    </div>    
                    
                    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="deleteModalTitle">Избриши</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    Дали сте сигурни дека сакате да го избришете производот?
                                </div>
                                <form class="modal-footer" id="delete-form" method="POST" action="">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Откажи</button>
                                    <button type="submit" class="btn btn-light">Избриши</button>
                                    @csrf
                                </form>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
    
        </div>
    </div>
</main>
@endsection