@extends('layouts.meetPetMaster')
@section('content')
<div class="container">
    <div class="row text-center search-by-filters">
        <form action="{{route('filter')}}" method="POST">
            {{ csrf_field() }}
            <div class="col-md-4">
                <p class="h3 white">TYPE</p>
                <div class="form-group">
                    <select name="type" id="type" class="select-dropdown form-control">
                            @foreach($types as $type)
                            <option value="{{$type->id}}" >{{$type->name}}</option>
                            @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <p class="h3 white">LOCATION</p>
                <div class="form-group">
                    <input type="text" name="location" id="location" class="form-control">
                </div>
            </div>
            <div class="col-md-4">
                <p class="h3 white">COATH LENGHT</p>
                <div class="form-group">
                    <select name="lenght" id="lenght" class="select-dropdown form-control">
                        @foreach($lenghts as $lenght)
                        <option value="{{$lenght->id}}" >{{$lenght->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <p class="h3 white">GENDER</p>
                <div class="form-group">
                    <select name="gender" id="gender" class="select-dropdown form-control">
                        @foreach($genders as $gender)
                        <option value="{{$gender->id}}" >{{$gender->type}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <p class="h3 white">SIZE</p>
                <div class="form-group">
                    <select name="size" id="size" class="select-dropdown form-control">
                            @foreach($sizes as $size)
                            <option value="{{$size->id}}" >{{$size->name}}</option>
                            @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <p class="h3 white">FIND YOUR NEW PET!</p>
                <div class="form-group">
                    <input type="submit" name="submit" value="Search" class="form-control">
                </div>
            </div>
        </form>
    </div>
    @isset ($search)
    <div class="pets-adoption">
        <div class="row cards-row">
            @foreach($results as $pet)
            <div class="col-lg-3 col-sm-4">
                <div class="grid">
                    <a href="/meet/{{$pet->id}}" >
                    <figure class="effect-zoe">
                        <div class="animal-image">
                            <img src="{{$pet->image}}">
                        </div>
                        <div class="panel-clip"></div>
                        <div class="animal-name purplecolor">
                            <p>{{$pet->name}}</p>
                        </div>
                        <figcaption class="text-center figcaption">
                            <div class="image-on-hover">
                                <img src="{{$pet->image}}" class="circle-image" alt="">
                            </div>
                            <div class="text-on-hover">
                                <p class="h4 purplecolor">{{$pet->name}}</p>
                            <p>{{$pet->short_description}}</p>
                            </div>
                            <div class="social" style="display:none;">
                                <i class="fab fa-facebook-f"></i>
                                <i class="fab fa-twitter"></i>
                                <i class="fab fa-pinterest-p"></i>
                                <i class="far fa-envelope"></i>
                                <i class="far fa-copy"></i>
                                <p class="social-text"><b>Share this pet.</b></p>
                            </div>
                            <div class="hover-button" onclick="return false">
                                <div class="button share"><i id="share" class="fa fa-share purplecolor"></i></div>
                                <div class="button exit"><i id="close" class="fas fa-times" style="display: none;"></i></div>
                            </div>
                        </figcaption>
                    </figure>
                    </a>
            </div>
        </div>
        @endforeach
    </div>
    </div>
    @endisset
</div>
@endsection