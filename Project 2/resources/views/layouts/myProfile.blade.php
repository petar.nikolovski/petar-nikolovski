@extends('layouts.meetPetMaster')
@section('content')
<section>
    <div class="cotainer container-padding">
                <section class="whitebackground">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <p class="h1 myProfile">My Profile</p>
                        @if (\Session::has('updated'))
                        <div class="alert alert-success">
                            <ul>
                                <li>{!! \Session::get('updated') !!}</li>
                            </ul>
                        </div>
                        @endif
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#aboutMe">About me</a></li>
                            <li><a data-toggle="tab" href="#accountSettings">Account Settings</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="aboutMe" class="tab-pane fade in active">
                                <div class="row">
                                    <div class="col-md-12 margin-top15px">
                                        <p class="h4">WHAT IS YOUR NAME?</p>
                                    </div>
                                </div>
                                <form action="/update/info/{{$user->id}}" method="POST">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group margin-top15px">
                                            <input type="text" name="firstname" id="firstname" value="{{$user->firstname}}" class="form-control" required>
                                                <label class="form-control-placeholder" for="name">First name</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group margin-top15px">
                                                <input type="text" name="lastname" id="lastname" class="form-control" value="{{$user->lastname}}">
                                                <label class="form-control-placeholder" for="lastname">Last name</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <p class="h4" style="margin-top:0px;">HOW CAN YOU BE REACHED?</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 margin-top15px">
                                            <div class="form-group">
                                                <input type="number" name="phonenumber" id="phoneNumber" value="{{$user->phone_number}}" class="form-control">
                                                <label class="form-control-placeholder" for="phoneNumber">Phone Number</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <p class="h4" style="margin-top:0px">WHERE DO YOU LIVE?</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group margin-top15px">
                                                <select class="form-control" name="country">
                                                    @foreach($countries as $country)
                                                        <option value={{$country->id}}>{{$country->name}}</option>
                                                    @endforeach
                                                </select>
                                                <label class="form-control-placeholder" for="country">Country(required)</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row margin-top15px">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                            <input type="text" name="address" id="address" class="form-control" value="{{$user->street_address}}">
                                                <label class="form-control-placeholder" for="address">Street Address</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" name="addressContinued" id="addressContinued" value="{{$user->street_address_continued}}" class="form-control">
                                                    <label class="form-control-placeholder" for="addressContinued">Street Address(continued)</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" name="city" id="city" class="form-control margin-top15px" value="{{$user->city}}">
                                                <label class="form-control-placeholder" for="city">City/Town</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group margin-top15px">
                                                <input type="text" name="ZIP" id="ZIP" class="form-control" value="{{$user->postal_code}}">
                                                <label class="form-control-placeholder" for="ZIP">ZIP code</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="submit" name="submit" value="UPDATE INFORMATION" class="btn btn-block btn-custom">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <a href="/add/pet" class="btn btn-block btn-custom">
                                                ADD A PET</a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="row">
                                    <div class="col-md-12 text-center padding-bot1rem">
                                        <p>You may search <a href="/find/pet/filter" class="myProfileFooter">our database</a> of
                                            thousands of pets looking for forever home</p>
                                    </div>
                                </div>
                            </div>
                            <div id="accountSettings" class="tab-pane fade">
                                    @if (\Session::has('success'))
                                    <div class="alert alert-success" style="margin-top:20px">
                                        <ul>
                                            <li>{!! \Session::get('success') !!}</li>
                                        </ul>
                                    </div>
                                    @endif
                                    @if (\Session::has('error'))
                                    <div class="alert alert-danger" style="margin-top:20px">
                                        <ul>
                                            <li>{!! \Session::get('error') !!}</li>
                                        </ul>
                                    </div>
                                    @endif
                                    @if ($errors->has('newpass'))
                                    <div class="alert alert-danger" style="margin-top:20px">
                                        <ul>
                                            <li>{{ $errors->first('newpass')}}</li>
                                        </ul>
                                    </div>
                                    @endif
                                
                                <div class="row">
                                    <div class="col-md-6 margin-top15px">
                                        <p class="h4">EMAIL</p>
                                    </div>
                                </div>
                                @if($errors->has('newEmail'))
                                <div class="alert alert-danger" style="margin-top:20px">
                                    <ul>
                                        <li>{{ $errors->first('newEmail')}}</li>
                                    </ul>
                                </div>
                                @endif
                                <div class="row">
                                    <div class="col-md-6 margin-top15px">
                                        <p data-toggle="collapse" data-target="#changeEmail" style="cursor:pointer">{{$user->email}}<span class="pull-right changeLink">Change</span></p>
                                        <div id="changeEmail" class="collapse">
                                            <form action="/update/email/{{$user->id}}" method="POST">
                                                {{ csrf_field() }}
                                                <div class="form-group">
                                                    <input type="email" placeholder="New Email" name="newEmail" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <input type="submit" name="submitEmail" value="UPDATE EMAIL" class="btn btn-custom btn-block">
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 margin-top15px">
                                        <p class="h4">CHANGE PASSWORD</p>
                                    </div>
                                </div>
                                <form method="POST" action="/update/password/{{$user->id}}">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="password" name="currentpass" id="currentpass" class="form-control margin-top15px" required>
                                                <label class="form-control-placeholder" for="currentpass">Current password</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="password" name="newpass" id="newpass" class="form-control margin-top15px" required>
                                                <label class="form-control-placeholder" for="newpass">New password</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="password" name="confirmpass" id="confirmpass" class="form-control margin-top15px" required>
                                                <label class="form-control-placeholder" for="confirmpass">Confirm password</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                            <div class="col-md-6 padding-bot1rem">
                                                    <div class="form-group">
                                                        <input type="submit" name="submit" class="btn btn-block btn-custom"
                                                            value="UPDATE PASSWORD">
                                                    </div>
                                                </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
    </div>
</section>
@endsection