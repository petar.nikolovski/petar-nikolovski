 <!-- background -->
 <section class="img">
    <div class="container">
        <div class="row">
            <div class="col-md-12 whitecolor text-center background-text">
                <p class="background-text-header bold">Where The Pets Finds Their People</p>
                <p>Thousands of adoptable pets are looking for people. People Like You.</p>
                <button class="background-button"><b>LEARN MORE</b></button>
            </div>
        </div>
    </div>
</section>
<!-- finds -->
<div class="container text-center">
    <div class="row row-fa-fonts">
        <div class="col-md-offset-1 col-md-5 box1 whitecolor">
            <input class="search-input" type="text" placeholder="Enter City, State or ZIP">
        </div>
        <a href="/find/pet/filter"><div class="col-md-3 col-sm-6 col-xs-6 box2 whitecolor">
            <p class="h4 box2-text"><i class="fas fa-dog"></i></p>
            <span class="find-text">Find a dog.</span>
        </div></a>
        <a href="/find/pet/filter"><div class="col-md-3 col-sm-6 col-xs-6 box3 whitecolor">
            <p class="h4"><i class="fas fa-cat"></i></p>
            <span class="find-text">Find a cat.</span>
        </div></a>
    </div>
</div>