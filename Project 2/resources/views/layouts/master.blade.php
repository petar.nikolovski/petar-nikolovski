<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
        integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link href='http://fonts.googleapis.com/css?family=Ubuntu&subset=cyrillic,latin' rel='stylesheet' type='text/css' />
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css'
        integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
    <link rel="stylesheet" href="css/landing.css">
    <link rel="stylesheet" href="css/loginRegisterModals.css">
    <link rel="stylesheet" type="text/css" href="css/cards.css">
    <link rel="stylesheet" type="text/css" href="css/header.css" >
    <link rel="stylesheet" type="text/css" href="css/article.css" >
    <link rel="stylesheet" type="text/css" href="css/footer.css" >
    <link rel="stylesheet" type="text/css" href="css/guide.css" >
    <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
    

</head>
<body>
    @include('layouts.header')
    @include('layouts.mainIntro')
    @include('auth.login')
    @include('auth.register')
    @include('layouts.cards')
    @include('layouts.guide')
    @include('layouts.cards')
    @include('layouts.article')
    @include('layouts.footer')
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
    integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
    crossorigin="anonymous"></script>
    <script src="style/owl.carousel.min.js"></script>
<script src="js/navigation.js"></script>
<script src="js/modal.js"></script>
<script src="js/cards.js"></script>
@if($errors->has('firstname') || $errors->has('lastname') || $errors->has('postalcode') || $errors->has('emailRegister') || $errors->has('registrationPassword'))
<script type='text/javascript'>
    $(window).on('load',function(){
        $('#myModal2').modal('show');
    });
</script>
@endif
@if($errors->has('passwordLogin') || $errors->has('email'))
<script type='text/javascript'>
    $(window).on('load',function(){
        $('#myModal1').modal('show');
    });
</script>
@endif
</html>