<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-3 footer-text-edit">
                        <h4 class="colorWhite bold ">ABOUT US</h4>
                        <a href="#">FAQs</a>
                        <a href="#">Partnerships</a>     
                        <a href="#">Terms of Service</a>
                        <a href="#">Mobile Site & Apps</a>
                        <a href="#">Foundation</a>
                        <a href="#">Free Widgets & Grephics</a>
                        <a href="#">Press</a>
                        <a href="#">For Developers</a>
                        <a href="#">Contact Us</a>
                    </div>
                    <div class="col-md-3 footer-text-edit">
                        <h4 class="colorWhite bold">PET ADOPTION</h4>
                        <a href="#">Dog Adoption</a>
                        <a href="#">Cat Adoption</a>
                        <a href="#">Other Pet Adoption</a>
                        <a href="#">Search Adoption Organisations</a>
                        <a href="#">Pet-Adoption Stories</a>
                        <a href="#">Local Adoption Events</a>
                        <a href="#">Shelters & Rescues</a>
                    </div>
                    <div class="col-md-3 footer-text-edit">
                        <h4 class="colorWhite bold">PET CARE TOPICS</h4>
                        <a href="#">Dog Care</a>
                        <a href="#">Dog Breeds</a>
                        <a href="#">Cat Care</a>
                        <a href="#">Cat Breeds</a>
                        <a href="#">All Pet Care</a>
                        <a href="#">Pet Care Videos</a>
                        <a href="#">Helping Pets</a>
                    </div>
                    <div class="col-md-3 colorWhite footer-items ">
                        <a class="h4 bold">SITEMAP</a>
                        <a class="h4 bold">PRIVACY POLICY</a>
                        <a class="h4 bold">ABOUT OUR ADS</a>
                        <a class="h4 bold">SHELTER & RESCUE REGISTRATION</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                    <p class="colorWhite h5">To get the latest on pet adoption and pet care, sign up for the Petfinder newsletter.</p>
                <div class="input-group">
                    <input type="email" class="form-control emailInputPadding " placeholder="Email" aria-describedby="basic-addon3">
                    <span class="input-group-addon" id="basic-addon2"><i class="fa fa-angle-right" style="font-size:20px;color:#7C53B4"></i></span>
                </div>   
            </div>
        </div>
    </div>
</footer>
<div class="hidden-footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 text-center">
                <p class="colorWhite bold h4">To get the latest on pet adoption and pet care, sign up for the Petfinder newsletter.</p>
                <button class="footer-register-button">SIGN UP</button>
            </div>
        </div>
        <div class="row hidden-footer-items">
            <div class="col-sm-4 col-xs-4">
                <p class="colorWhite bold h4">ABOUT US</p>
                <a href="#">FAQs</a>
                <a href="#">Partnerships</a>     
                <a href="#">Terms of Service</a>
                <a href="#">Mobile Site & Apps</a>
                <a href="#">Foundation</a>
                <a href="#">Free Widgets & Grephics</a>
                <a href="#">Press</a>
                <a href="#">For Developers</a>
                <a href="#">Contact Us</a>
            </div>
            <div class="col-sm-4 col-xs-4">
                <p class="colorWhite bold h4">PET ADOPTION</p>
                <a href="#">Dog Adoption</a>
                <a href="#">Cat Adoption</a>
                <a href="#">Other Pet Adoption</a>
                <a href="#">Search Adoption Organisations</a>
                <a href="#">Pet-Adoption Stories</a>
                <a href="#">Local Adoption Events</a>
                <a href="#">Shelters & Rescues</a>
            </div>
            <div class="col-sm-4 col-xs-4">
                <p class="colorWhite bold h4">PET CARE ADOPTION</p>
                <a href="#">Dog Care</a>
                <a href="#">Dog Breeds</a>
                <a href="#">Cat Care</a>
                <a href="#">Cat Breeds</a>
                <a href="#">All Pet Care</a>
                <a href="#">Pet Care Videos</a>
                <a href="#">Helping Pets</a>
            </div>
        </div>
        <div class="row last-row-items">
            <div class="col-sm-3">
                <a class="bold">SITEMAP</a>
            </div>
            <div class="col-sm-3">
                <a class="bold">PRIVACY POLICY</a>
            </div>
            <div class="col-sm-3">
                <a class="bold">ABOUT OUR ADS</a>
            </div>
            <div class="col-sm-3">
                <a class="bold last-child">SHELTER & RESCUE REGISTRATION</a>
            </div>
        </div>
    </div>
</div>
<div class="social-media-footer">
    <div class="container-fluid">
            <div class="row">
                    <div class="col-md-6 col-sm-3 col-xs-2">
                        <p class="footer-text">©2019</p>
                    </div>
                    <div class="col-md-6 col-sm-9 col-xs-10 text-right social-media-fonts">
                            <i class="fab fa-facebook-f" style="font-size:22px; "></i>
                            <i class="fab fa-twitter" style="font-size:22px; "></i>
                            <i class="fab fa-instagram" style="font-size:22px;"></i>
                            <i class="fab fa-youtube" style="font-size:22px;"></i>
                            <i class="fab fa-pinterest-p" style="font-size:22px;"></i>
                    </div>
            </div>
    </div>
</div>

