<div class="modal  bs-example-modal-lg" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                    <div class="col-md-6 col-sm-4 firstHalfForm">
                        <p class="h3 padding-bottom22px">We Make Adopting Easier</p>
                        <ul style="padding:0px;padding-left:20px;">
                            <li class="padding-bottom22px">Create and save your adopter profile</li>
                            <li class="padding-bottom22px">Save and manage your pet searches and email
                                communications.</li>
                            <li class="padding-bottom22px">Learn helpful pet care tips and receive expert advice.
                            </li>
                            <li>Get involved and help promote adoptable pets in your area.</li>
                        </ul>
                    </div>
                    <form role="form" action='#' method="post">
                        <div class="col-md-6 col-sm-8 secondHalfForm">
                            <p class="h3 padding-bottom20px">Log in</p>
                            <div class="form-group">
                                <input type="email" name="email" class="form-control" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" class="form-control" placeholder="Password">
                            </div>
                            <div class="form-group">
                                <input type="submit" name="login" class="btn btn-block btn-custom" value="LOG IN">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-custom btn-block">
                                    <i class="fab fa-facebook-f"></i> SIGN IN WITH FACEBOOK
                                </button>
                            </div>
                            <a href="#">
                                <p class="text-center purple" style="margin:0;">FORGOT PASSWORD?</p>
                            </a>
                            <hr class="hrStyle">
                            <p style="font-size:13px;" class="text-center margin-bottom15px font-weightBold">DON'T HAVE
                                AN ACCOUNT?</p>
                            
                                <button data-toggle="modal" class="btn btn-block btn-custom" data-target="#myModal2" type="button" id="registerNow">REGISTER
                                    NOW</button>
                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal  bs-example-modal-lg" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg modal-margin">
        <div class="modal-content">
        <form role="form" action='{{route('createuser')}}' method="POST">
                <div class="modal-body">
                    <button type="button" class="close marginRight-5px" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <div class="row">
                        <div class="col-md-12" style="margin-bottom:5px">
                            <p class="h3 modal-title" id="myLargeModalLabel">Register</p>
                            @foreach ($errors->all() as $error)
                                <div>{{ $error }}</div>
                            @endforeach
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" name="firstname" class="form-control" placeholder="Name" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" name="lastname" class="form-control" placeholder="Last Name"
                                    required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <select name="country" class="form-control" required>
                                    @foreach ($country as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" name="postalcode" class="form-control" placeholder="Postal Code"
                                    required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="email" name="emailRegister" class="form-control" placeholder="Email"
                                    required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4">
                                    <p class="font-size12px" style="margin:0;">You will use
                                        your email to log in</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="password" name="passwordRegister" class="form-control"
                                    placeholder="Password" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="password" name="confirmPassRegister" class="form-control"
                                    placeholder="Confirm Password" required>
                            </div>
                        </div>
                    </div>
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group padding-bottom15px" style="padding:8px">
                                <input type="submit" name="register" class="btn btn-block btn-custom"
                                    value="REGISTER NOW">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>