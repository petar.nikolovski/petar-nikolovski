
<div class="guide">
        <div class="container">
                <div class="row  guide-fonts">
                  <div class="col-md-12">
                      <p class="h1 text-center purplecolor margin35px">Planning to Adopt a Pet ?</p>
                  </div>
                  <div class="col-md-4 text-center">
                        <i class="fas fa-book"></i>
                        <p class="h3 bold guide-info" style="color:rgb(40, 83, 224);">Checklist for New Adopters</p>
                        <p class="fontsize12px">Help make the transition, as smooth as possible.</p>
                        <button type="button" name="checklistButton" class="checklistButton">LEARN MORE</button>
                  </div>
                  <div class="col-md-4 text-center">
                        <i class="fas fa-binoculars"></i>
                        <p class="h3 bold guide-info " style="color:rgb(40, 83, 224)">Finding the Right Pet</p>
                        <p class="fontsize12px">Get tips on figuring out who you should adopt.</p>
                        <button type="button" name="findingRightPetButton" class="checklistButton">LEARN MORE</button>
                  </div>
                  <div class="col-md-4 text-center">
                        <i class="fas fa-group"></i>
                        <p class="h3 bold guide-info " style="color:rgb(40, 83, 224)">Pet Adoption FAQs</p>
                        <p class="fontsize12px">Get answers to questions you haven't thought of.</p>
                        <button type="button" name="FAQsButton" class="checklistButton">LEARN MORE</button>
                  </div>
                </div>
        </div>
</div>

<section class="small-device-carousel">
            <div class="container-fluid carousel">
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                          <!-- Indicators -->
                          <ol class="carousel-indicators">
                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel" data-slide-to="1"></li>
                            <li data-target="#myCarousel" data-slide-to="2"></li>
                          </ol>
                      
                          <!-- Wrapper for slides -->
                          <div class="carousel-inner text-center">
                            <div class="item active">
                              <i class="fas fa-book"></i>
                                  <p class="h3 bold guide-info" style="color:rgb(40, 83, 224);">Checklist for New Adopters</p>
                                  <p class="fontsize12px">Help make the transition, as smooth as possible.</p>
                                  <button type="button" name="checklistButton" class="checklistButton">LEARN MORE</button>
                            </div>
                      
                            <div class="item">
                              <i class="fas fa-binoculars"></i>
                              <p class="h3 bold guide-info " style="color:rgb(40, 83, 224)">Finding the Right Pet</p>
                              <p class="fontsize12px">Get tips on figuring out who you should adopt.</p>
                              <button type="button" name="findingRightPetButton" class="checklistButton">LEARN MORE</button>
                            </div>
                          
                            <div class="item">
                              <i class="fas fa-group"></i>
                              <p class="h3 bold guide-info " style="color:rgb(40, 83, 224)">Pet Adoption FAQs</p>
                              <p class="fontsize12px">Get answers to questions you haven't thought of.</p>
                              <button type="button" name="FAQsButton" class="checklistButton">LEARN MORE</button>
                            </div>
                          </div>
                      
                          <!-- Left and right controls -->
                          <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                            <span class="sr-only">Previous</span>
                          </a>
                          <a class="right carousel-control" href="#myCarousel" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                            <span class="sr-only">Next</span>
                          </a>
                        </div>
            </div>
</section>
