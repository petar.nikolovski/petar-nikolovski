@extends('layouts.meetPetMaster')
@section('content')

    <!-- INTRO -->
    <section class="blackBackground">
        <div class="container">
            <div class="row relative">
                <div class="col-md-offset-3 col-md-6">
                    <img src="{{$pets->image}}" width="100%">
                </div>
                <div class="col-md-3 absolute">
                    <i class="fas fa-expand openModalButton" data-toggle="modal" data-target="#photoModal"></i>
                    <!-- Modal -->
                    <div id="photoModal" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <img src="{{$pets->image}}" width="100%">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- MAIN CONTENT -->
    <section class="mainContentMarginTopBot">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-7 col-xs-12 whitebackground" style="margin:0">
                    <p class="h2 purple bold">
                        {{$pets->name}}
                    </p>
                    <p class="h5">
                        {{$pets->location}}
                    </p>
                    <hr>
                    <p class="h5">
                        {{$pets->genders->type}} • {{$pets->sizes->name}}
                    </p>
                    <hr>
                    <p class="h2 purple bold">
                        About
                    </p>
                    <p class="h5 bold">
                        COAT LENGTH
                    </p>
                    <p class="h5">
                        {{$pets->lenghts->name}}
                    </p>
                    <p class="h5 bold">
                        HOUSE TRAINED
                    </p>
                    <p class="h5">
                        {{$pets->is_trained_models->name}}
                    </p>
                    <p class="h5 bold">
                        SHORT DESCRIPTION
                    </p>
                    <p class="h5">
                        {{$pets->short_description}}
                    </p>
                    <p class="h5 bold">
                        GOOD IN A HOME WITH
                    </p>
                    <p class="h5">
                        {{$pets->good_with}}
                    </p>
                    <hr>
                    <p class="h2 purple bold">
                        Meet {{$pets->name}}
                    </p>
                    <p class="h5">
                        {{$pets->long_description}}
                    </p>
                </div>
                <div class="col-md-1 col-sm-1"></div>
                <div class="col-md-4 col-sm-4 col-xs-12 purple-background text-center">
                    <a href="/about/{{$pets->id}}"><button class="btn-ask">ASK ABOUT {{$pets->name}}</button></a>
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6 print-share"><i class="fas fa-share"></i>SHARE</div>
                        <div class="col-md-6 col-sm-6 col-xs-6 print-share"><i class="fas fa-print"></i>PRINT</div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
@endsection