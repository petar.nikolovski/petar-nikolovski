@extends('layouts.meetPetMaster')
@section('content')
@foreach ($errors->all() as $error)
    <p style="color:#333">Mistake: {{$error}}</p>
@endforeach
<section>
<div class="container">
    <div class="row">
        <div class="col-md-offset-1 col-md-10 whitebackground padding-top1rem">
        <form action="/add/pet" method="POST" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {{ csrf_field() }}
                        <label for="pet">Pet Name</label>
                        <input type="text" name="pet_name" id="pet" class="form-control" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="type">Type</label>
                        <select class="form-control" name="type">
                            @foreach ($types as $type)
                                <option value="{{$type->id}}">{{$type->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="location">Location</label>
                        <input type="text" name="location" id="location" class="form-control" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="pet_image">Image</label>
                        <input type="file" name="pet_image" id="pet_image">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="short_description">Short Description</label>
                        <input type="text" name="short_description" id="short_description" class="form-control"
                            required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="characteristic">Characteristics</label>
                        <input type="text" name="characteristics" id="characteristics" class="form-control" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="coat_lenght">Coat Lenght</label>
                        <select class="form-control" name="coat_lenght">
                            @foreach ($lenghts as $lenght)
                                <option value="{{$lenght->id}}">{{$lenght->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="house_trained">House Trained</label>
                        <select class="form-control" name="house_trained">
                            @foreach ($is_trained_models as $is_trained_model)
                                <option value="{{$is_trained_model->id}}">{{$is_trained_model->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <label for="good_with">Good with</label>
                        <input type="text" name="good_with" id="good_with" class="form-control" required>
                    </div>
                </div>
            </div>
            <div class="row">
                    <div class="col-md-6">
                            <div class="form-group">
                                <label for="size">Size</label>
                                <select class="form-control" name="size">
                                    @foreach ($sizes as $size)
                                        <option value="{{$size->id}}">{{$size->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="gender">Gender</label>
                        <select class="form-control" name="gender">
                            @foreach ($genders as $gender)
                                <option value="{{$gender->id}}">{{$gender->type}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                        <div class="form-group">
                                <label for="long_description">Long Description</label>
                                <textarea type="text" name="long_description" id="long_description" class="form-control" required></textarea>
                            </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 text-center padding-bot1rem">
                        <div class="form-group">
                                <input type="submit" name="submit" class="btn btn-block btn-custom"
                                    value="ADD A PET">
                            </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
</section>
@endsection