<div class="modal  bs-example-modal-lg" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                    <div class="col-md-6 firstHalfForm">
                        <p class="h3 padding-bottom22px">We Make Adopting Easier</p>
                        <ul style="padding:0px;padding-left:20px;">
                            <li class="padding-bottom22px">Create and save your adopter profile</li>
                            <li class="padding-bottom22px">Save and manage your pet searches and email
                                communications.</li>
                            <li class="padding-bottom22px">Learn helpful pet care tips and receive expert advice.
                            </li>
                            <li>Get involved and help promote adoptable pets in your area.</li>
                        </ul>
                    </div>
                    <form role="form" action="{{ route('login') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="col-md-6 secondHalfForm">
                            <p class="h3 padding-bottom20px">Log in</p>
                            @if($errors->has('email') || $errors->has('password'))
                                <p style="color:red">These credentials do not match our records!</p>
                            @endif
                            <div class="form-group">
                                <input type="email" name="email" class="form-control" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" class="form-control" placeholder="Password">
                            </div>
                            <div class="form-group">
                                <input type="submit" name="login" class="btn btn-block btn-custom" value="LOG IN">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-custom btn-block">
                                    <i class="fab fa-facebook-f"></i> SIGN IN WITH FACEBOOK
                                </button>
                            </div>
                            <a href="#">
                                <p class="text-center purple" style="margin:0;"><a href="#">FORGOT PASSWORD?</a></p>
                            </a>
                            <hr class="hrStyle">
                            <p style="font-size:13px;" class="text-center margin-bottom15px font-weightBold">DON'T HAVE
                                AN ACCOUNT?</p>
                            
                                <button data-toggle="modal" class="btn btn-block btn-custom" data-target="#myModal2" type="button" id="registerNow">REGISTER
                                    NOW</button>
                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>