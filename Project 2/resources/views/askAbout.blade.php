@extends('layouts.meetPetMaster')
@section('content')
<div class="purple-section">
    <div class="container">
        <div class="row">
            <div class="col-md-2 pet-info">
                <img src="{{$pets->image}}" alt="">
            </div>
            <div class="col-md-10 pet-description">
                <p class="white bold h3">Ask About {{$pets->name}}</p>
                <p class="white h4"> {{$pets->genders->type}} • {{$pets->sizes->name}}</p>
            </div>
        </div>
    </div>
</div>
<div class="container ask-about-pet">
    <div class="row">
        <div class="col-md-6">
            @if(!Auth::check())
            <p class="h5">Have an account? <span class="purple bold">Sign In</span></p>
            <p class="h5 bold">OR INQUIRE AS A GUEST</p>
            @else
            <p class="h5">From</p>
            <p class="h5 bold">{{Auth::user()->firstname}}</p>
            @endif
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group margin-top15px">
                        <input type="text" name="firstname" class="form-control" required>
                            <label class="form-control-placeholder">First name</label>
                    </div>
                    <div class="form-group margin-top15px">
                        <input type="text" name="email" class="form-control" required>
                            <label class="form-control-placeholder">Email</label>
                    </div>
                    <div class="form-group margin-top15px">
                        <input type="text" name="country" class="form-control" required>
                            <label class="form-control-placeholder">Country</label>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group margin-top15px">
                        <input type="text" name="lastname" class="form-control" required>
                            <label class="form-control-placeholder">Last name</label>
                    </div>
                    <div class="form-group margin-top15px">
                        <input type="text" name="phone_number" class="form-control" required>
                            <label class="form-control-placeholder">Phone number</label>
                    </div>
                    <div class="form-group margin-top15px">
                        <input type="text" name="postal_code" class="form-control" required>
                            <label class="form-control-placeholder">Postal code</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <p class="h5"><span class="bold">OUR MESSAGE (5000 CHARACTER LIMIT)</span> <span class="gray">Additional Comments for the Shelter/Rescue</span></p>
            <textarea class="text-area" name="" id="" cols="30" rows="8" placeholder="I'm wondering if {{$pets->name}} is..."></textarea>
            <button class="send-message-button bold white">Send Message</button>
        </div>
    </div>
</div>
@endsection