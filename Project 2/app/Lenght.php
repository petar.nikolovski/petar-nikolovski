<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lenght extends Model
{
    public function pets()
    {
        return $this->hasOne(Pet::class, 'lengths_id');
    }
}
