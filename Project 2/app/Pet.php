<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pet extends Model
{
    public function users()
    {
        return $this->belongsTo(User::class);
    }

    public function types()
    {
        return $this->belongsTo(Type::class);
    }

    public function genders()
    {
        return $this->belongsTo(Gender::class);
    }

    public function is_trained_models()
    {
        return $this->belongsTo(IsTrainedModel::class);
    }

    public function lenghts()
    {
        return $this->belongsTo(Lenght::class, 'lengths_id');
    }

    public function sizes()
    {
        return $this->belongsTo(Size::class);
    }
}
