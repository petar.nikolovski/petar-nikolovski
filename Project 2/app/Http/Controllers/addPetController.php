<?php

namespace App\Http\Controllers;
use App\Http\Requests\addPetRequest;
use App\Http\Requests\FilterRequest;
use \App\Type;
use \App\Lenght;
use \App\Size;
use \App\IsTrainedModel;
use \App\Gender;
use \App\Pet;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;

class addPetController extends Controller
{
    public function addPet()
    {
        $types = Type::all();
        $lenghts = Lenght::all();
        $sizes = Size::all();
        $is_trained_models = IsTrainedModel::all();
        $genders = Gender::all(); 
        return view('addPet', compact('types', 'lenghts', 'sizes', 'is_trained_models', 'genders'));
    }
    public function addPetPost(addPetRequest $request)
    {
        $pet = new Pet;
        $pet->name = $request->get('pet_name');
        $pet->types_id = $request->get('type');
        $pet->location = $request->get('location');
        // ADD IMAGE LOCALY
        $pet->image = $request->file('pet_image');
        $pet->image->move(public_path('/css/images'), time() . "." . $pet->image->getClientOriginalExtension());
        $pet->image ="/css/images/" . time() . "." . $pet->image->getClientOriginalExtension();
        // END OF ADD IMAGE LOCALY
        $pet->short_description = $request->get('short_description');
        $pet->characteristics = $request->get('characteristics');
        $pet->users_id = rand(1,5);
        $pet->lengths_id = $request->get('coat_lenght');
        $pet->is_trained_models_id = $request->get('house_trained');
        $pet->good_with = $request->get('good_with');
        $pet->sizes_id = $request->get('size');
        $pet->genders_id = $request->get('gender');
        $pet->long_description = $request->get('long_description');
        $pet->save();
        return redirect("/meet/$pet->id");
    }
    public function meetPet($id)
    {
        if(Pet::find($id))
        {
            $pets = Pet::with('sizes', 'types', 'is_trained_models', 'genders', 'lenghts')->find("$id");
            return view('meetPet', compact('pets'));
        }
        return redirect('/landingpage');
    }
    public function aboutPet($id)
    {
        $pets = Pet::findOrFail($id);
        return view('askAbout', compact('pets'));
    }
    public function findPet(Pet $pet)
    {
        $search = Input::all();
        $genders = Gender::all();
        $lenghts = Lenght::all();
        $sizes = Size::all();
        $types = Type::all();
        if($search === [])
        {
            return view('meetThemFilter', compact('genders', 'sizes', 'lenghts', 'types'));
        }
        else
        {
            $pet = $pet->newQuery();

            if (Input::has('type')) {
                $pet->where('types_id', Input::get('type'));
            }
            if (!Input::has('location')) {
                $pet->where('location', Input::get('location'));
            }
            if (Input::has('lenght')) {
                $pet->where('lengths_id', Input::get('lenght'));
            }
            if (Input::has('gender')) {
                $pet->where('genders_id', Input::get('gender'));
            }
            if (Input::has('size')) {
                $pet->where('sizes_id', Input::get('size'));
            }

            // Get the results and return them.
            $results = $pet->get();
            return view('meetThemFilter', compact('genders', 'sizes', 'lenghts', 'types', 'results', 'search'));
        }
    }
}
