<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Country;
use \App\Pet;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function landingpage()
    {
        $pets = Pet::all();
        $country = Country::all();
        $random = Pet::all()->random(4);
        return view('landingpage', compact('country', 'pets', 'random'));
    }
    public function logout()
    {
        Auth::logout();
        return redirect('/landingpage');
    }
}
