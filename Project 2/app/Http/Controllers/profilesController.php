<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Http\Middleware\CheckIsLoggedIn;
use App\Http\Requests\UpdatePassword;
use App\Http\Requests\UpdateInfo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\ChangeEmail;
use \App\User;
use \App\Country;
use Illuminate\Support\Facades\Input;

class profilesController extends Controller
{
    public function __construct() {
        $this->middleware('loggedIn');
    }
    public function profile($id)
    {
        $user = User::findOrFail($id);
        if(Auth::user()->id == $id)
        {
            $countries = Country::all();
            return view('layouts.myProfile', compact('user', 'countries'));
        }
        return redirect()->back();
    }
    public function updatePassword(UpdatePassword $request, $id)
    {
        $user = User::findorFail($id);
        if(Hash::check($request->get('currentpass'), $user->password))
        {
        $user->password = bcrypt($request->get('newpass'));
        $user->save();
        return redirect()->back()->with('success', 'Password has been updated!'); 
        }
        return redirect()->back()->with('error', 'Thats not your current password!');
    }
    public function updateInfo(UpdateInfo $request, $id)
    {
        $user = User::findOrFail($id);
        $user->firstname = $request->get('firstname');
        $user->lastname = $request->get('lastname');
        $user->phone_number = $request->get('phonenumber');
        $user->countries_id = $request->get('country');
        $user->street_address = $request->get('address');
        $user->street_address_continued = $request->get('addressContinued');
        $user->city = $request->get('city');
        $user->postal_code = $request->get('ZIP');
        $user->save();
        if($user->save())
        {
            return redirect()->back()->with('updated', 'Your information has been updated!');
        }
    }
    public function updateEmail(ChangeEmail $request, $id)
    {
        $user = User::findOrFail($id);
        $user->email = $request->get('newEmail');
        $user->save();
        return redirect()->back();
    }
}
