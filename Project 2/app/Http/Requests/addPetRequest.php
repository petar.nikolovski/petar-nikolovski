<?php

namespace App\Http\Requests;
use Illuminate\Http\Request;

use Illuminate\Foundation\Http\FormRequest;

class addPetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pet_name' => 'required',
            'type' => 'required',
            'location' => 'required',
            'pet_image' => 'required',
            'short_description' => 'required',
            'characteristics' => 'required',
            'coat_lenght' =>'required',
            'house_trained' => 'required',
            'good_with' => 'required',
            'size' => 'required',
            'gender' => 'required',
            'long_description' => 'required'
        ];
    }
}