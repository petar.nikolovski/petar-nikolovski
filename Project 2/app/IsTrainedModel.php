<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IsTrainedModel extends Model
{
    public function pets()
    {
        return $this->hasOne(Pet::class);
    }
}
