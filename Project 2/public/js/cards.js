$(document).ready(function(){
    $('.share').on('click',function(){
        $(this).parent().parent().parent().find( ".figcaption" ).css({"background-color":"rgb(139, 92, 206)", "transition":".6s"});
        $(this).parent().parent().parent().find(".fa-times").css({"display": "unset", "color": "#74C53B4", "font-size": "26px", "position": "absolute", "right":"12px"});
        $(this).parent().parent().parent().find(".fa-share").css("display", "none");
        $(this).parent().parent().parent().find(".text-on-hover").css("display", "none");
        $(this).parent().parent().parent().find(".social").css("display", "unset");
    })
    $('.exit').on('click',function(){
        $(this).parent().parent().parent().find( ".figcaption" ).css({"background-color":"white", "transition":".3s"});
        $(this).parent().parent().parent().find(".fa-times").css("display", "none");
        $(this).parent().parent().parent().find(".fa-share").css("display", "unset");
        $(this).parent().parent().parent().find(".text-on-hover").css("display", "unset");
        $(this).parent().parent().parent().find(".social").css("display", "none");
    })
})
