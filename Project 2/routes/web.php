<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/landingpage');
});
// HOME CONTROLLER
Route::get('/landingpage')->name('landingpage')->uses('HomeController@landingpage');
// ADD PET CONTROLLER
Route::get('/add/pet')->name('add.pet')->uses('addPetController@addPet')->middleware('loggedIn');
Route::post('/add/pet')->name('add.pet.post')->uses('addPetController@addPetPost');
Route::get('/meet/{id}')->name('meet.pet')->uses('addPetController@meetPet');
Route::get('/about/{id}')->name('about.pet')->uses('addPetController@aboutPet');
Route::get('/find/pet/filter')->name('find.pet')->uses('addPetController@findPet');
Route::post('/find/pet/filter')->name('filter')->uses('addPetController@findPet');
// AUTH
Auth::routes();
Route::get('/logout', 'HomeController@logout')->name('logout');
// PROFILES CONTROLLER
Route::get('/profile', function () {
    $id = Illuminate\Support\Facades\Auth::user()->id;
    return redirect("/profile/$id");
})->middleware('loggedIn');
Route::get('/profile/{id}')->name('profile')->uses('profilesController@profile');
Route::post('/update/info/{id}')->name('update.info')->uses('profilesController@updateInfo');
Route::post('/update/password/{id}')->name('update.user.password')->uses('profilesController@updatePassword');
Route::post('/update/email/{id}')->name('update.email')->uses('profilesController@updateEmail');
