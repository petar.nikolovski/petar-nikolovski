<?php

use Illuminate\Database\Seeder;

class Types_table_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr = ['Dog', 'Cat','Rabbit', 'Bird'];
        for($i=0;$i<count($arr);$i++)
        {
            $put = new \App\Type;
            $put->name = $arr[$i];
            $put -> save();
        }
    }
}
