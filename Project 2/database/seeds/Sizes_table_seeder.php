<?php

use Illuminate\Database\Seeder;

class Sizes_table_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr = ['Big', 'Medium','Small'];
        for($i=0;$i<count($arr);$i++)
        {
            $put = new \App\Size;
            $put->name = $arr[$i];
            $put->save();
        }
    }
}
