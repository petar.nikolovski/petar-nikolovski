<?php

use Illuminate\Database\Seeder;

class Is_trained_models_table_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $trained = ['Yes', 'No'];
        for($i=0;$i<count($trained);$i++)
        {
            $is_trained = new \App\IsTrainedModel;
            $is_trained->name = $trained[$i];
            $is_trained->save();
        }
    }
}
