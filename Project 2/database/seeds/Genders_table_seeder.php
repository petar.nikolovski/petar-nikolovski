<?php

use Illuminate\Database\Seeder;

class Genders_table_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $genders = ['male', 'female'];
        for($i=0;$i<count($genders);$i++)
        {
        $gender = new \App\Gender;
        $gender->type = $genders[$i];
        $gender->save();
        }
    }
}