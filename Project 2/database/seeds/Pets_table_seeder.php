<?php

use Illuminate\Database\Seeder;
use \App\Pet;

class Pets_table_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = ["Puster", "Milco", "Cutie", "Timmy"];
        $types = [1, 1, 2, 2];
        $locations = ["Kumanovo", "Skopje", "Kriva Palanka", "Bitola"];
        $images = ["/css/images/1557678139.jpg", "/css/images/1557678303.jpg", "/css/images/catBro.jpg", "/css/images/22.jpg"];
        $short_desc = ["The pug is a breed of dog with physically distinctive features of a wrinkly.", "Although dogs look very different from people.", "Just like you clip and file your nails so they don’t reach Guinness Record lengths.", "Cats have the same 5 senses as people do but to very different degrees."];
        $characs = ["Lazy, sleepy", "Fun, filled with energy, hungry all the time", "Lazy, loves to push things", "Cute, lovely"];
        $users_id = [4, 5, 1, 1];
        $lengths = [2, 1, 3, 2];
        $is_trained = [1, 1, 1, 2];
        $good_with = ["People and other dogs", "People and other dogs", "Nobody", "People and other cats"];
        $sizes_id = [3, 1, 2, 2];
        $genders_id = [1, 1, 2, 2];
        $long_desc = ["Pugs often are described as a lot of dog in a small space. These sturdy, compact dogs are a part of the American Kennel Club’s Toy group, and are known as the clowns of the canine world because they have a great sense of humor and like to show off.", "They have a heart and circulatory system to transport blood, lungs to take in oxygen and rid the body of carbon dioxide, a digestive tract to absorb nutrients from food, and so on. However, it is the differences between dogs and people that are most interesting and that give dogs their unique characteristics as family members.", "One cat behavior is by scratching, which helps us remove dead nail growth. There are two other important explanations behind why we scratch: We do it to mark our territory (we’ve got scent glands on our paws—how cool is that?) or to stretch (how do you think we stay so graceful?)", "Cats have keen vision; they can see much more detail than dogs. Concentrated in the center of the retina of the eye, a specific type of cell called a cone gives cats excellent visual acuity and binocular vision. This allows them to judge speed and distance very well, an ability that helped them survive as hunters."];
        for($i = 0; $i < count($names); $i++)
        {
            $pet = new Pet;
            $pet->name = $names[$i];
            $pet->types_id = $types[$i];
            $pet->location = $locations[$i];
            $pet->image = $images[$i];
            $pet->short_description = $short_desc[$i];
            $pet->characteristics = $characs[$i];
            $pet->users_id = $users_id[$i];
            $pet->lengths_id = $lengths[$i];
            $pet->is_trained_models_id = $is_trained[$i];
            $pet->good_with = $good_with[$i];
            $pet->sizes_id = $sizes_id[$i];
            $pet->genders_id = $genders_id[$i];
            $pet->long_description = $long_desc[$i];
            $pet->save();
        }
    }
}
