<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;


class Countries_table_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0;$i<5;$i++)
        {
        $faker = Faker::create();
        $country = new \App\Country;
        $country->name = $faker->country;
        $country->save();
        }
    }
}
