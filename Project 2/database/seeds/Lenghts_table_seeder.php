<?php

use Illuminate\Database\Seeder;

class Lenghts_table_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr = ['Long', 'Medium','Short'];
        for($i=0;$i<count($arr);$i++)
        {
            $put = new \App\Lenght;
            $put->name = $arr[$i];
            $put->save();
        }
    }
}
