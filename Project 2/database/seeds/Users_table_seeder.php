<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;


class Users_table_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0;$i<5;$i++)
        {
        $faker = Faker::create();
        $user = new \App\User;
        $user->firstname = $faker->firstName;
        $user->lastname = $faker->lastName;
        $user->countries_id = rand(1,5);
        $user->email = $faker->email;
        $user->password = $faker->password;
        $user->phone_number = rand(5000,10000);
        $user->street_address = $faker->address;
        $user->city = $faker->city;
        $user->postal_code = rand(1000,3000);
        $user->save();
        }
    }
}