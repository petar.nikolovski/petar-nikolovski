<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(Types_table_seeder::class);
        $this->call(Genders_table_seeder::class);
        $this->call(Sizes_table_seeder::class);
        $this->call(Lenghts_table_seeder::class);
        $this->call(Is_trained_models_table_seeder::class);
        $this->call(Countries_table_seeder::class);
        $this->call(Users_table_seeder::class);
        $this->call(Pets_table_seeder::class);
    }
}
