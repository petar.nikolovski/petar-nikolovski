<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePetsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
      Schema::create('pets', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->string('name');
          $table->unsignedBigInteger('types_id');
          $table->foreign('types_id')->references('id')->on('types');
          $table->string('location');
          $table->text('image');
          $table->text('short_description');
          $table->string('characteristics');
          $table->unsignedBigInteger('users_id');
          $table->foreign('users_id')->references('id')->on('users');
          $table->unsignedBigInteger('lengths_id');
          $table->foreign('lengths_id')->references('id')->on('lenghts');
          $table->unsignedBigInteger('is_trained_models_id');
          $table->foreign('is_trained_models_id')->references('id')->on('is_trained_models');
          $table->string('good_with');
          $table->unsignedBigInteger('sizes_id');
          $table->foreign('sizes_id')->references('id')->on('sizes');
          $table->unsignedBigInteger('genders_id');
          $table->foreign('genders_id')->references('id')->on('genders');
          $table->text('long_description');
          $table->timestamps();
      });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::dropIfExists('pets');
  }
}