<footer>
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <p class="bold h1 footer-header">Дали сте заинтересирани вашата компанија да понуди попуст ?</p>
            <p class="footer-subheader h5">Најдобри онлајн попусти за производи, услуги, фитнес центри, ресторани, едукација и кариера.</p>
            <a href="<?php echo e(route('discount-for-companies')); ?>"><button class="footer-button">Креирај попуст</button></a>
        </div>
    </div>
</div>
</footer>