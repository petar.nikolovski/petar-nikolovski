<?php $__env->startSection('content'); ?>
    <div class="yellow-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <a href="/"><i class="fa fa-arrow-left"></i></a>
                    <p class="bold header h1">Купи картичка за вработените</p>
                </div>
            </div>
        </div>
    </div>
    <div class="gray-bg">
        <div class="container">
            <form id="data_employees">
                <div class="success alert alert-success">
                    <span class="success_message"></span>
                </div>
                <div class="row">
                    <?php echo e(csrf_field()); ?>

                    <div class="col-md-12">
                        <p class="bold h4">Име и презиме</p>
                        <input type="text" placeholder="пр. Петар Николовски" name="full_name" id="full_name">
                        <p class="small full_name_error error"></p>
                        <p class="bold h4">Име на компанија</p>
                        <input type="text" placeholder="пр. Brainster" name="company_name" id="company_name">
                        <p class="small company_name_error error"></p>
                    </div>
                    <div class="col-md-6">
                        <p class="bold h4">Број на вработени</p>
                        <input type="text" placeholder="пр. 200" name="number_of_emp" id="number_of_emp">
                        <p class="small number_of_emp_error error"></p>
                    </div>
                    <div class="col-md-6">
                        <p class="bold h4">Телефон за контакт</p>
                        <input type="text" placeholder="пр. +38971376434" name="phone_number" id="phone_number">
                        <p class="small phone_number_error error"></p>
                    </div>
                    <div class="col-md-12">
                        <button id="send_form" class="submit-button">Испрати</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.employee-master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>