<?php $__env->startSection('content'); ?>
    <div class="yellow-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <a href="/"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
                    <p class="bold header h1">Купи картичка за вработените</p>
                </div>
            </div>
        </div>
    </div>
    <div class="gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="bold h4">Име и презиме</p>
                    <input type="text" placeholder="пр. Петар Николовски" name="full-name">
                    <p class="bold h4">Име на компанија</p>
                    <input type="text" placeholder="пр. Brainster" name="company-name">
                </div>
                <div class="col-md-6">
                    <p class="bold h4">Број на вработени</p>
                    <input type="text" placeholder="пр. 200" name="number-of-emp">
                </div>
                <div class="col-md-6">
                    <p class="bold h4">Телефон за контакт</p>
                    <input type="text" placeholder="пр. 07X/XXX-XXX" name="number">
                </div>
                <div class="col-md-12">
                    <button class="submit-button">Испрати</button>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.employee-master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>