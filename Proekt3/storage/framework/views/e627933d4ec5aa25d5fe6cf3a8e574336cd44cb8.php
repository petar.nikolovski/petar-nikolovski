<div class="light-gray">
    <div class="container">
        <div class="row three-in-row">
            <?php $__currentLoopData = $companies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $company): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-md-4 card-column cards <?php echo e($company->category->id); ?>" id="">
                    <div class="card text-center">
                        <div class="logo">
                            <img src="<?php echo e($company->logo); ?>" alt="">
                        </div>
                        <div class="content">
                            <p class="bold h5 company"><?php echo e($company->name); ?></p>
                            <p class="yellow-text"><?php echo e($company->discount_type); ?></p>
                            <button class="category-button"><?php echo e($company->category->name); ?></button>
                            <p><span><i class="fa fa-thumbs-up" aria-hidden="true"></i></span><span><i class="fa fa-thumbs-down" aria-hidden="true"></i></span></p>
                            <a href="/show/<?php echo e($company->id); ?>"><button class="see-more-btn">Види повеќе</button></a>
                        </div>
                    </div>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
        <div class="row one-in-row">
            <?php $__currentLoopData = $companies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $company): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
               <div class="col-md-12">
                    <div class="card cards <?php echo e($company->category->id); ?>" id="">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="changed-logo">
                                    <img src="<?php echo e($company->logo); ?>" alt="">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <p class="bold h4 company-name company"><?php echo e($company->name); ?></p>
                                <p class="yellow-text company-desc"><?php echo e($company->discount_type); ?></p>
                                <button class="category-button"><?php echo e($company->category->name); ?></button>
                            </div>
                            <div class="col-md-4 company-info text-center">
                                <a href="/show/<?php echo e($company->id); ?>"><button class="see-more-btn">Види повеќе</button></a>
                                <p><span><i class="fa fa-thumbs-up" aria-hidden="true"></i></span><span><i class="fa fa-thumbs-down" aria-hidden="true"></i></span></p>
                            </div>
                            <input type="integer" hidden id="<?php echo e($company->id); ?>">
                        </div>
                    </div>
               </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
</div>
