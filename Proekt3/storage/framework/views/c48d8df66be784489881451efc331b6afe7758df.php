<?php $__env->startSection('content'); ?>
    <div class="gray-bg">
        <div class="container">
            <?php $__currentLoopData = $companies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $company): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if($company->id == $id): ?>
                    <div class="row text-center">
                        <a href="/"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
                        <div class="col-md-12">
                            <img class="logo" src="<?php echo e($company->logo); ?>" alt="">
                        </div>
                        <div class="col-md-12 mt-2">
                            <button class="category-button"><?php echo e($company->category->name); ?></button>
                        </div>
                        <div class="col-md-12 mb-5">
                            <h3 class="company-header">Попуст во <?php echo e($company->name); ?>- <?php echo e($company->discount_type); ?></h3>
                        </div>
                    </div>
                    <hr>
                    <div class="row mt-5">
                        <div class="col-md-7">
                            <div class="row">
                                <!--Carousel Wrapper-->
                                    <div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails text-center" data-ride="carousel">
                                        <!--Slides-->
                                        <div class="carousel-inner" role="listbox">
                                            <div class="carousel-item active">
                                                <img src="<?php echo e($company->logo); ?>"
                                                     alt="First slide">
                                            </div>
                                            <?php $__currentLoopData = $images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if($image->company_id == $company->id): ?>
                                                    <div class="carousel-item">
                                                        <img src="<?php echo e($image->path); ?>"
                                                             alt="First slide">
                                                    </div>
                                                <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </div>
                                        <!--/.Slides-->
                                        <!--Controls-->
                                        <a class="carousel-control-prev" href="#carousel-thumb" role="button" data-slide="prev">
                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="carousel-control-next" href="#carousel-thumb" role="button" data-slide="next">
                                            <span class="carousel-control-next-icon" style="color: black;" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>

                                        <!--/.Controls-->

                                    </div>
                                    <!--/.Carousel Wrapper-->
                            </div>
                            <h6 class="description"><?php echo e($company->description); ?></h6>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-4 black-bg">
                            <div class="row">
                                <div class="col-md-12 mb-4">
                                    <p><span><i class="fa fa-thumbs-up" aria-hidden="true"></i></span><span><i
                                                    class="fa fa-thumbs-down" aria-hidden="true"></i></span></p>
                                </div>
                                <div class="col-md-12 mb-1">
                                    <p class="yellow-text">E-mail</p>
                                    <p class="white-text"><?php echo e($company->email); ?></p>
                                </div>
                                <div class="col-md-12">
                                    <p class="yellow-text">Физичка адреса</p>
                                    <p class="white-text"><?php echo e($company->address); ?></p>
                                </div>
                                <div class="col-md-12 mb-1">
                                    <p class="yellow-text">Фејсбук страна</p>
                                    <p class="white-text"><a style="color: white;"
                                                             href="<?php echo e($company->fb_page); ?>"><?php echo e($company->fb_page); ?></a></p>
                                </div>
                                <div class="col-md-12 mb-1">
                                    <p class="yellow-text">Телефонски број</p>
                                    <p class="white-text"><?php echo e($company->phone_number); ?></p>
                                </div>
                                <div class="col-md-12 mt-1">
                                    <p class="yellow-text">Google Maps адреса</p>
                                    <iframe src="<?php echo e($company->google_maps_address); ?>"
                                            frameborder="0" style="border:0" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>

    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.panel-master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>