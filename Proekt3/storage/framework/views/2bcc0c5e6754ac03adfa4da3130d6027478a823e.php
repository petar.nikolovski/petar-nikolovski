<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2 ">
            <div class="panel panel-default mb-5">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <?php if(session('status')): ?>
                        <div class="alert alert-success">
                            <?php echo e(session('status')); ?>

                        </div>
                    <?php endif; ?>
                    <?php if(session('aprove')): ?>
                        <div class="alert alert-success">
                            <?php echo e(session('aprove')); ?>

                        </div>
                    <?php endif; ?>
                    <?php if(session('delete')): ?>
                        <div class="alert alert-danger">
                            <?php echo e(session('delete')); ?>

                        </div>
                    <?php endif; ?>

                   <p> You are logged in!</p>
                        <a class="home" href="/">Go back at landing page !</a>
                </div>

            </div>
        </div>
        <div class="col-md-12">
            <h2>Компании кои што аплицирале за картичка за вработени</h2>
            <table class="table table-dark">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Full Name</th>
                    <th scope="col">Company Name</th>
                    <th scope="col">Number of Emplyoees</th>
                    <th scope="col">Phone Number</th>
                </tr>
                </thead>
                <tbody>
                <?php $__currentLoopData = $employees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $employee): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <th scope="row"><?php echo e($employee->id); ?></th>
                    <td scope="row"><?php echo e($employee->full_name); ?></td>
                    <td scope="row"><?php echo e($employee->company_name); ?></td>
                    <td scope="row"><?php echo e($employee->number_of_emp); ?></td>
                    <td scope="row"><?php echo e($employee->phone_number); ?></td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
        <div class="col-md-12">
            <h2>Компании кои што нудат картички со попуст</h2>
        </div>
    </div>
    <div class="row three-in-row">
        <?php $__currentLoopData = $companies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $company): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-md-4 card-column cards <?php echo e($company->category->id); ?>" id="">
                <div class="card text-center">
                    <div class="logo">
                        <img src="<?php echo e($company->logo); ?>" alt="">
                    </div>
                    <div class="content">
                        <p class="bold h4 company"><?php echo e($company->name); ?></p>
                        <p class="yellow-text"><?php echo e($company->discount_type); ?></p>
                        <button class="category-button"><?php echo e($company->category->name); ?></button>
                        <a href="/show/<?php echo e($company->id); ?>"><button class="see-more-btn">Види повеќе</button></a>
                        <p><a class="actions" href="/aprove/<?php echo e($company->id); ?>">Одобри</a><a class="actions" href="/edit/<?php echo e($company->id); ?>">Измени</a><a class="actions" href="/delete/<?php echo e($company->id); ?>">Избриши</a></p>
                    </div>
                </div>
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>