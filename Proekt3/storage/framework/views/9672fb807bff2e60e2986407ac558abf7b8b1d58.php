<div class="gray-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-7 mb-2">
                <input class="search-input" id="search" type="text" placeholder="Пребарувај попусти">
            </div>
            <div class="col-md-3">
                <select name="" id="option" class="dropdown">
                    <option selected="true" id="all">Сите</option>
                    <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($category->id); ?>"><?php echo e($category->name); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>
            </div>
            <div class="col-md-2">
                <span><i class="fa fa-th active" aria-hidden="true"></i></span>
                <span><i class="fa fa-list"></i></span>
            </div>
        </div>
    </div>
</div>