<div class="yellow-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <p class="bold header h1">Купете картичка со попуст за вашите вработени.</p>
                <p class="gray sub-header h5">Најдобри онлајн попусти за производи, услуги, фитнес центри, ресторани, едукација и кариера.</p>
                <a href="<?php echo e(route('discount-for-emp')); ?>"><button class="buy-now-button">Купи сега</button></a>
            </div>
        </div>
    </div>
</div>