<?php $__env->startSection('content'); ?>
    <div class="yellow-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <a href="/"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
                    <p class="bold header h1">Понудете картичка со попуст</p>
                </div>
            </div>
        </div>
    </div>
    <div class="gray-bg">
        <div class="container">
            <div class="row">
                <?php if(session()->has('message')): ?>
                    <div class="col-md-12">
                        <div class="success alert alert-success">
                            <span class="success_message"><?php echo e(session()->get('message')); ?></span>
                        </div>
                    </div>
                <?php endif; ?>
                <form method="POST" enctype="multipart/form-data" action="<?php echo e(route('saveForm')); ?>">
                    <?php echo e(csrf_field()); ?>

                    <div class="col-md-12">
                        <p class="bold h4">Име на компанија која нуди попуст</p>
                        <input type="text" placeholder="пр. Мајкрософт ДООЕЛ" name="name" value="<?php echo e(old('name')); ?>">
                        <?php if($errors->has('name')): ?>
                            <p class="small error"><?php echo e($errors->first('name')); ?></p>
                        <?php endif; ?>

                        <p class="bold h4">Вид на попуст</p>
                        <input type="text" placeholder="пр. 50% на купување" name="discount_type" value="<?php echo e(old('discount_type')); ?>">
                        <?php if($errors->has('discount_type')): ?>
                            <p class="small error"><?php echo e($errors->first('discount_type')); ?></p>
                        <?php endif; ?>
                        <p class="bold h4">Избери категорија</p>
                        <div class="boxed">
                            <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <input type="radio"  name="category_id" id="<?php echo e($category->id); ?>" value="<?php echo e($category->id); ?>">
                                <label for="<?php echo e($category->id); ?>"><?php echo e($category->name); ?></label>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                        <?php if($errors->has('category_id')): ?>
                            <p class="small error"><?php echo e($errors->first('category_id')); ?></p>
                        <?php endif; ?>
                        <p class="bold h4">Постави thumbnail(logo)</p>
                        <img src="" id="profile-img-tag"/>
                        <i class="fa fa-close" id="logo-close"></i>
                        <div class="add-logo">
                            <div class="get_file"><i class="fa fa-plus" aria-hidden="true"></i></div>
                            <input type="file" class="my_file" id="logo-img" name="logo" style="display: none;" value="<?php echo e(old('logo')); ?>">
                        </div>
                        <?php if($errors->has('logo')): ?>
                            <p class="small error"><?php echo e($errors->first('logo')); ?></p>
                        <?php endif; ?>
                        <p class="bold h4">Опис на понудата</p>
                        <textarea id="textarea" name="description" cols="30" rows="6" placeholder="Краток опис за вашата понуда"><?php echo e(old('description')); ?></textarea>
                        <?php if($errors->has('description')): ?>
                            <p class="small error"><?php echo e($errors->first('description')); ?></p>
                        <?php endif; ?>
                        <p class="bold h4">Линк до вашиот веб-сајт</p>
                        <input type="text" placeholder="пр. https://mojvebsajt.com" name="web_site" value="<?php echo e(old('web_site')); ?>">
                        <?php if($errors->has('web_site')): ?>
                            <p class="small error"><?php echo e($errors->first('web_site')); ?></p>
                        <?php endif; ?>
                        <p class="bold h4">Фејсбук страна</p>
                        <input type="text" placeholder="пр. https://facebook.com/my_company" name="fb_page" value="<?php echo e(old('fb_page')); ?>">
                        <?php if($errors->has('fb_page')): ?>
                            <p class="small error"><?php echo e($errors->first('fb_page')); ?></p>
                        <?php endif; ?>
                        <p class="bold h4">Телефонски број</p>
                        <input type="text" placeholder="пр. +38971376434" name="phone_number" value="<?php echo e(old('phone_number')); ?>">
                        <?php if($errors->has('phone_number')): ?>
                            <p class="small error"><?php echo e($errors->first('phone_number')); ?></p>
                        <?php endif; ?>
                        <p class="bold h4">Вашиот мејл</p>
                        <input type="email" placeholder="пр. mojotmail@gmail.com" name="email" value="<?php echo e(old('email')); ?>">
                        <?php if($errors->has('email')): ?>
                            <p class="small error"><?php echo e($errors->first('email')); ?></p>
                        <?php endif; ?>
                        <p class="bold h4">Google maps адреса</p>
                        <input type="text" placeholder="пр. https://google.com/maps/place/your+place" name="google_maps_address" value="<?php echo e(old('google_maps_address')); ?>">
                        <?php if($errors->has('google_maps_address')): ?>
                            <p class="small error"><?php echo e($errors->first('google_maps_address')); ?></p>
                        <?php endif; ?>
                        <p class="bold h4">Физичка адреса</p>
                        <input type="text" placeholder="пр. ул. Партизански херои бб." name="address" value="<?php echo e(old('address')); ?>">
                        <?php if($errors->has('address')): ?>
                            <p class="small error"><?php echo e($errors->first('address')); ?></p>
                        <?php endif; ?>
                        <p class="bold h4">Фотографии(1-8 слики)</p>
                        <div class="add-gallery">
                            <div class="get_file_gallery"><i class="fa fa-plus" aria-hidden="true"></i></div>
                            <input type="file" class="my_file_gallery" name="gallery[]" id="fileupload"  multiple="multiple" style="display: none;">
                        </div>
                        <div class="col-md-12 text-right">
                            <i class="fa fa-close" id="gallery-close" style="display:none;"></i>
                        </div>
                        <div class="col-md-12">
                            <div class="addGallery"></div>
                        </div>

                        <?php if($errors->has('gallery[]')): ?>
                            <p class="small error"><?php echo e($errors->first('gallery[]')); ?></p>
                        <?php endif; ?>
                        <button class="submit-button" type="submit">Испрати</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.company-master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>