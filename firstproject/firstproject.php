<?php 
include 'php/array.php';
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Brainster Tool Box</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
    <link rel="stylesheet" type="text/css" media="screen" href="style/firstproject.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="style/mediaquery.css"/>
</head>

<body>

    <!-- navigation bar -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <div class="w3-sidebar w3-bar-block w3-animate-left" style="display:none;z-index:5" id="mySidebar">
                    <img class="pull-left logo-edit" src="images/logo.png">
                    <button class="w3-bar-item w3-button w3-large pull-right close-button" onclick="w3_close()">&times;
                        Затвори</button>
                    <a class="sidenav-padding my-yellow border-bottom bold w3-bar-item w3-button" href="register.php" target="_blank">Регистрирај се</a>
                    <a class="my-yellow border-bottom bold w3-bar-item w3-button" href="login.php" target="_blank">Најави се</a>
                    <a class="black-color border-bottom bold w3-bar-item w3-button" href="aboutUs.html" target="_blank">За нас</a>
                    <a class="black-color border-bottom bold w3-bar-item w3-button" href="https://www.facebook.com/pg/brainster.co/photos/" target="_blank">Галерија</a>
                    <a class="black-color border-bottom bold w3-bar-item w3-button" href="contact.html" target="_blank">Контакт</a>
                </div>
                <div class="w3-overlay w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" id="myOverlay"></div>
                <div>
                    <i class="fa fa-bars my-yellow" style="font-size:20px;cursor:pointer; color:#f9d42;" onclick="w3_open()">
                        Мени</i>
                    <a href="#goto"><img class="img-background" src="images/logo.png"></a>
                </div>
            </div>
            <a href="https://www.brainster.io/business" target="_blank"><button class="btn first-button custom-btn-edit my-yellow bold pull-right orginal-button">Вработи
                    наши студенти</button></a>
            <a href="https://www.brainster.io/business" target="_blank"><button class="btn second-button black-color custom-btn-edit bold pull-right orginal-button">Обуки
                    за компании</button></a>
        </div>
    </nav>

    <!-- main div -->
    <section class="main-div jumptarget clippy-div" id="goto">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12 text-center custom-padding-text">
                    <p class="white">Изработено од студенти за програмирање на <span class="my-yellow sidenav-padding bold"><ins>Brainster</ins></span></p>
                    <a href="https://www.brainster.io/business" target="_blank"><button class="btn first-button custom-btn-edit my-yellow bold reserve-button">Вработи
                            наши студенти</button></a>
                    <a href="https://www.brainster.io/business" target="_blank"><button class="btn second-button black-color custom-btn-edit bold reserve-button">Обуки
                            за компании</button></a>
                </div>

                <div class="col-md-12">
                    <p class="header-edit bold white text-center">FUTURE-PROOF YOUR ORGANIZATION</p>
                    <div class="col-md-offset-2 col-md-8">
                        <p class="text-center white custom-padding-text text-edit">Find out how to unlock progress in
                            your
                            business. Understand your current state, indentify opportunity arreas and prepare to
                            take chareg of your organizations future.</p>
                    </div>
                    <div class="col-md-12 text-center">
                        <a href="https://brainsterquiz.typeform.com/to/kC2I9E" target="_blank"><button class="btn third-button black-color bold">Take the assessment</button></a></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- all games -->
    <section class="games games-margin">
            <div class="container">
                <div class="row">
                <?php
                for ($i = 0; $i < count($games); $i++) {
                    echo "
                            <div class = 'col-md-4 col-sm-6'>
                                <div class ='panel panel-default'>
                                <a href='gamepage.php?gameIndex=$i' style='color:unset;' target='_blank'>
                                <div class='panel-heading'><img src=" . $games[$i]['img'] . " width = '100%' height= '250px'; class='img-radius'></div>
                                    <div class='panel-body'>
                                        <div class='row'>
                                            <div class='col-md-9 col-sm-9 col-xs-9'>
                                                <p class='font-size-tittle bold'>" . $games[$i]['game-tittle'] . "</p>
                                                <p class='h6'>Категорија: <span class='aqua-blue-color bold'>" . $games[$i]['game-category'] . "</span></p>
                                            </div>
                                            <div class='col-md-3 col-sm-3 col-xs-3'>
                                                 <div class='game-logo-edit'>
                                                 <img src=" . $games[$i]['game-logo'] . "  class='pull-right' height='65px' style='padding:15px;'>
                                                 </div>
                                            </div>
                                        </div>
                                        <div class = 'row fa-padding'>
                                            <div class = 'col-md-1 col-xs-1'>
                                                <i class='far fa-clock' style='font-size:24px'></i>
                                            </div>
                                            <div class = 'col-md-11 col-xs-11'>
                                            <p class='bold'>Времетраење:" . "<br><span class='gray'>" . $games[$i]['game-time'] . "</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </a>
                            </div>";
                }
                ?>
                </div>
            </div>
    </section>

    <!-- lastdiv-text -->
    <section class="bg-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="header-edit bold white text-center custom-header-padding">FUTURE-PROOF YOUR ORGANIZATION</p>
                    <div class="col-md-offset-2 col-md-8">
                        <p class="text-center white custom-padding-text text-edit">Find out how to unlock progress in
                            your
                            business. Understand your current state, indentify opportunity arreas and prepare to
                            take chareg of your organizations future.</p>
                    </div>
                    <div class="col-md-12 text-center">
                        <a href="https://brainsterquiz.typeform.com/to/kC2I9E" target="_blank"><button class="btn fourth-button black-color bold">Take the assessment</button></a></a>
                    </div>
                </div>
            </div>
            </div>
    </section>

    <!-- footer -->
    <footer>
        <div class="container-fluid text-center">
            <div class="row row-margin">
                <div class="col-md-4 col-sm-4 col-margin bold">
                    <a href="aboutUs.html" target="_blank"><span class="white word-margin">About us</span></a>
                    <a href="contact.html" target="_blank"><span class="white word-margin">Contact</span></a>
                    <a href="https://www.facebook.com/pg/brainster.co/photos/" target="_blank"><span class="white">Gallery</span></a>
                </div>
                <div class="col-md-4 col-sm-4 img-margin">
                    <img class="img-footer" src="images/logo.png">
                </div>
                <div class="col-md-4 col-sm-4 col-margin">
                    <a href="https://www.linkedin.com/school/brainster-co/" target="_blank"><i class="fab fa-linkedin-in fafonts word-margin"></i></a>
                    <a href="https://twitter.com/BrainsterCo" target="_blank"><i class="fab fa-twitter fafonts word-margin"></i></a>
                    <a href="https://www.facebook.com/brainster.co" target="_blank"> <i class="fab fa-facebook-f fafonts"></i></a>
                </div>
            </div>
        <div class="row mobile-footer">
                <div class="col-md-12 mobile-footer-margin">
                    <a href="https://www.linkedin.com/school/brainster-co/" target="_blank"><i class="fab fa-linkedin-in fafonts word-margin"></i></a>
                    <a href="https://twitter.com/BrainsterCo" target="_blank"><i class="fab fa-twitter fafonts word-margin"></i></a>
                    <a href="https://www.facebook.com/brainster.co" target="_blank"> <i class="fab fa-facebook-f fafonts"></i></a>
                </div>
                <div class="col-md-12 mobile-footer-margin">
                    <img class="img-mobile" src="images/logo.png">
                </div>
                <div class="col-md-12">
                    <a href="aboutUs.html" target="_blank"><span class="white word-margin">About us</span></a>
                    <a href="contact.html" target="_blank"><span class="white word-margin">Contact</span></a>
                    <a href="https://www.facebook.com/pg/brainster.co/photos/" target="_blank"><span class="white">Gallery</span></a>
                </div>
        </div>
            </div>
    </footer>


</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script>
    function w3_open() {
        document.getElementById("mySidebar").style.display = "block";
        document.getElementById("myOverlay").style.display = "block";
    }

    function w3_close() {
        document.getElementById("mySidebar").style.display = "none";
        document.getElementById("myOverlay").style.display = "none";
    }
</script>
</html> 