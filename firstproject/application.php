<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Апликација</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
    <link rel="stylesheet" type="text/css" media="screen" href="style/application.css"/>
</head>
<body>
    <section class="register text-center">
        <div class="container text-center" style="background-color:#988888;">
        <?php         
        if(isset($_GET['success'])) {
            if($_GET['success'] == 'register') {
                echo "<p style='color:green' class='h4 text-center'><strong>Успешна апликација.<br><br><a href='firstproject.php'>Вратете се на почетната страна.</a></strong></p>";
            }
        }
        if(isset($_GET['error'])) {
            if($_GET['error'] == 'required') {
                echo "<p style='color:red' class='h4 text-center'>Сите полиња се задолжителни.</p>";
            }else {
            echo "<p style='color:red' class='h4 text-center'>Обидете се повторно</p>";
            }
        }
       
        ?>
            <h1 class="text-color header-margin">Апликација</h1>
            <p class="text-margin text-color">Пополнете ги сите полиња за да аплицирате.</p>
			<form action="php/applicationPHP.php" method="POST">
                <input type="text" name="firstname" class="input-text" placeholder="Име" required>
                <input type="text" name="lastname" class="input-text" placeholder="Презиме" required>
                <input type="text" name="company" class="input-text" placeholder="Компанија" required>
                <input type="text" name="email" class="input-text" placeholder="E-mail" required>
                <input type="text" name="phone" class="input-text" placeholder="Телефонски број" required><br>
                <select name="vraboteni">
                    <option value="">Изберете број на вработени</option>
                    <option value="1">1</option>
                    <option value="2-10">2-10</option>
                    <option value="11-50">11-50</option>
                    <option value="51-200">51-200</option>
                    <option value="200+">200+</option>
                </select><br>
                <select name="sektor">
                    <option value="">Изберете сектор:</option>
                    <option value="Covecki resursi">Човечки ресурси</option>
                    <option value="Marketing">Маркетинг</option>
                    <option value="Produkt">Продукт</option>
                    <option value="Prodazba">Продажба</option>
                    <option value="CEO">CEO</option>
                    <option value="Drugo">Друго</option>
                </select><br>
                <textarea name="commentar" cols="30" rows="10" placeholder="Коментар.."></textarea>
                <button type="submit" name="submit" class="registerbtn">Аплицирај</button>
            </form>
            <p class="edit-text text-color">Врати се на почетната страна-<a href="firstproject.php">Клик</a></p>
        </div>
    </section>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</html> 