<?php
$games = [
   
    //  1 karticka
    ["img" => "images/b1.jpeg",
    "game-tittle" => "Dotmocracy",
    "game-category" => "Акција",
    "game-logo" => "images/a1.png",
    "game-time" => "5 до 30 минути"],
    

    // 2 karticka
    ["img" => "images/b18.jpg",
    "game-tittle" => "Project Mid-way Evaluation",
    "game-category" => "Акција",
    "game-logo" => "images/a2.png",
    "game-time" => "30 до 60 минути"],
    

    // 3 karticka
    ["img" => "images/b3.jpg",
    "game-tittle" => "The 5 Whys",
    "game-category" => "Иновација",
    "game-logo" => "images/a3.png",
    "game-time" => "30 до 60 минути"],
    

    // 4 karticka
    ["img" => "images/b44.jpeg",
    "game-tittle" => "Future Trends",
    "game-category" => "Иновација",
    "game-logo" => "images/a4.png",
    "game-time" => "60 до 120 минути"],
    

    // 5 karticka
    ["img" => "images/b5.jpeg",
    "game-tittle" => "Story Building",
    "game-category" => "Иновација",
    "game-logo" => "images/a5.png",
    "game-time" => "30 до 60 минути"],
    

    // 6 karticka
    ["img" => "images/b66.jpg",
    "game-tittle" => "Тake a Stand",
    "game-category" => "Иновација",
    "game-logo" => "images/a6.png",
    "game-time" => "60 до 120 минути"],
    

    // 7 karticka
    ["img" => "images/b7.jpg",
    "game-tittle" => "IDOARRT Meeting Design",
    "game-category" => "Акција",
    "game-logo" => "images/a7.png",
    "game-time" => "5 до 30 минути"],
    

    // 8 karticka
    ["img" => "images/b17.jpg",
    "game-tittle" => "Action Steps",
    "game-category" => "Акција",
    "game-logo" => "images/a8.png",
    "game-time" => "120 до 240 минути"],
    

    // 9 karticka
    ["img" => "images/b9.jpeg",
    "game-tittle" => "Letter to Myself",
    "game-category" => "Лидерство",
    "game-logo" => "images/a9.png",
    "game-time" => "5 до 30 минути"],
    

    // 10 karticka
    ["img" => "images/b10.jpeg",
    "game-tittle" => "Аctive Listening",
    "game-category" => "Лидерство",
    "game-logo" => "images/a10.png",
    "game-time" => "60 до 120 минути"],
    

    // 11 karticka
    ["img" => "images/b11.jpg",
    "game-tittle" => "Feedback: I appreciate",
    "game-category" => "Лидерство",
    "game-logo" => "images/a11.png",
    "game-time" => "60 до 120 минути"],
    

    // 12 karticka
    ["img" => "images/b12.jpeg",
    "game-tittle" => "Explore your values",
    "game-category" => "Лидерство",
    "game-logo" => "images/a12.png",
    "game-time" => "60 до 120 минути"],
    

    // 13 karticka
    ["img" => "images/b13.jpg",
    "game-tittle" => "Reflection Individual",
    "game-category" => "Лидерство",
    "game-logo" => "images/a13.png",
    "game-time" => "30 до 60 минути"],
    

    // 14 karticka
    ["img" => "images/b14.jpeg",
    "game-tittle" => "Back-turned Feedback",
    "game-category" => "Лидерство",
    "game-logo" => "images/a14.png",
    "game-time" => "60 до 120 минути"],
    
    
    // 15 karticka
    ["img" => "images/b15.jpg",
    "game-tittle" => "Conflict Responses",
    "game-category" => "Тим",
    "game-logo" => "images/a15.png",
    "game-time" => "60 до 120 минути"],
    

    // 16 karticka
    ["img" => "images/b16.jpg",
    "game-tittle" => "Myers-Briggs Team Reflection",
    "game-category" => "Тим",
    "game-logo" => "images/a16.png",
    "game-time" => "60 до 120 минути"],
    

    // 17 karticka
    ["img" => "images/b8.jpeg",
    "game-tittle" => "Personal Presentations",
    "game-category" => "Тим",
    "game-logo" => "images/a17.png",
    "game-time" => "60 до 240 минути"],
    

    // 18 karticka
    ["img" => "images/b2.jpeg",
    "game-tittle" => "Circles of influence",
    "game-category" => "Тим",
    "game-logo" => "images/a18.png",
    "game-time" => "30 до 120 минути"]
    
]
?>