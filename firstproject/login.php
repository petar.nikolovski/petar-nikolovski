<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Најава</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
    <link rel="stylesheet" type="text/css" media="screen" href="style/login.css"/>
</head>
<body>
    <section class="register text-center">
        <div class="container text-center" style="background-color:#988888;">
        <?php
        if(isset($_GET['error'])) {
            echo "<p style='color:red;font-weight:bold;' class='h4 text-center'>";
            if($_GET['error'] == 'required') {
                echo "Пополни ги сите полиња!";
            }
            if($_GET['error'] == 'wrong') {
                echo "Погрешено корисничко име или лозинка!";
            }
            echo "</p>";
        }
        ?>
            <h1 class="text-color header-margin">Најави се</h1>
            <p class="text-margin text-color">Пополнете ги сите полиња за да се најавите.</p>
			<form action="php/loginPHP.php" method="POST">
            	<input type="text" name="username" id="usernamename" class="input-text" placeholder="Kорисничко име" required>
            	<input type="password" name="pass" id="comfirm-password" class="input-text" placeholder="Лозинка" required>
            	<button type="submit" name="submit" class="registerbtn">Најави се</button>
            </form>
            <p class="edit-text text-color">Доколку немате креирано профил <a href="register.php"> Регистрирај се</a></p>
            <p class="edit-text text-color">Врати се на почетната страна-<a href="firstproject.php">Клик</a></p>
        </div>
    </section>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</html> 