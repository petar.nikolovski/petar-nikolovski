<?php $__env->startSection('content'); ?>

    <!-- INTRO -->
    <section class="blackBackground">
        <div class="container">
            <div class="row relative">
                <div class="col-md-offset-3 col-md-6">
                    <img src="<?php echo e($pets->image); ?>" width="100%">
                </div>
                <div class="col-md-3 absolute">
                    <i class="fas fa-expand openModalButton" data-toggle="modal" data-target="#photoModal"></i>
                    <!-- Modal -->
                    <div id="photoModal" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <img src="<?php echo e($pets->image); ?>" width="100%">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- MAIN CONTENT -->
    <section class="mainContentMarginTopBot">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-7 col-xs-12 whitebackground" style="margin:0">
                    <p class="h2 purple bold">
                        <?php echo e($pets->name); ?>

                    </p>
                    <p class="h5">
                        <?php echo e($pets->location); ?>

                    </p>
                    <hr>
                    <p class="h5">
                        <?php echo e($pets->genders->type); ?> • <?php echo e($pets->sizes->name); ?>

                    </p>
                    <hr>
                    <p class="h2 purple bold">
                        About
                    </p>
                    <p class="h5 bold">
                        COAT LENGTH
                    </p>
                    <p class="h5">
                        <?php echo e($pets->lenghts->name); ?>

                    </p>
                    <p class="h5 bold">
                        HOUSE TRAINED
                    </p>
                    <p class="h5">
                        <?php echo e($pets->is_trained_models->name); ?>

                    </p>
                    <p class="h5 bold">
                        SHORT DESCRIPTION
                    </p>
                    <p class="h5">
                        <?php echo e($pets->short_description); ?>

                    </p>
                    <p class="h5 bold">
                        GOOD IN A HOME WITH
                    </p>
                    <p class="h5">
                        <?php echo e($pets->good_with); ?>

                    </p>
                    <hr>
                    <p class="h2 purple bold">
                        Meet <?php echo e($pets->name); ?>

                    </p>
                    <p class="h5">
                        <?php echo e($pets->long_description); ?>

                    </p>
                </div>
                <div class="col-md-1 col-sm-1"></div>
                <div class="col-md-4 col-sm-4 col-xs-12 purple-background text-center">
                    <button class="btn-ask"><a href="/about/<?php echo e($pets->id); ?>">ASK ABOUT <?php echo e($pets->name); ?></a></button>
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6 print-share"><i class="fas fa-share"></i>SHARE</div>
                        <div class="col-md-6 col-sm-6 col-xs-6 print-share"><i class="fas fa-print"></i>PRINT</div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.meetPetMaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>