
<!-- navigation-bar -->
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container-fluid">
            <div class="row row-navbar">
                <div class="col-md-8 col-sm-7 col-xs-3">
                    <div class="navbar-header">
                        <ul class="nav navbar-nav navbar-right navbar-account">
                            <li>
                                <a class="hamburger-menu" id="nav-icon3" role="button" data-toggle="collapse"
                                    href="#main-navigation" aria-expanded="false" aria-controls="main-navigation">
                                    <svg class="ham hamRotate ham1" viewBox="0 0 100 100" width="60" style="margin-top:4px;"
                                        onclick="this.classList.toggle('active')">
                                        <path class="line top"
                                            d="m 30,33 h 40 c 0,0 9.044436,-0.654587 9.044436,-8.508902 0,-7.854315 -8.024349,-11.958003 -14.89975,-10.85914 -6.875401,1.098863 -13.637059,4.171617 -13.637059,16.368042 v 40" />
                                        <path class="line middle" d="m 30,50 h 40" />
                                        <path class="line bottom"
                                            d="m 30,67 h 40 c 12.796276,0 15.357889,-11.717785 15.357889,-26.851538 0,-15.133752 -4.786586,-27.274118 -16.667516,-27.274118 -11.88093,0 -18.499247,6.994427 -18.435284,17.125656 l 0.252538,40" />
                                    </svg>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-1 col-sm-1">
                    <div class="search text-right">
                        <i class="fa fa-search" aria-hidden="true"></i>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-9 border-left">
                    <div class="register-signin">
                        <?php if(!Auth::check()): ?>
                        <span data-toggle="modal" data-target="#myModal1"><i class="fa fa-user"></i>Sign In / Register</span>
                        <?php else: ?>
                        <a href="/profile/<?php echo e(Auth::user()->id); ?>"><i class="fa fa-user"><span class="welcome-span">Welcome <?php echo e(Auth::user()->firstname); ?></span></i></a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

        </div>
        <!--MAIN NAV-->
        <div id="main-navigation" class="collapse main-nav-toggle">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 ul-text-align">
                        <ul>
                            <li><a href="/landingpage">HOME</a></li>
                            <?php if(Auth::check()): ?>
                            <li><a href="/profile/<?php echo e(Auth::user()->id); ?>">MY PROFILE</a></li>
                            <?php endif; ?>
                            <li><a href="/find/pet/filter">DOG ADOPTION</a></li>
                            <li><a href="/find/pet/filter">CAT ADOPTION</a></li>
                            <li><a href="#">PET ADOPTION STORIES</a></li>
                            <li><a href="#">VIDEOS</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--END MAIN NAV-->
    </nav>
    <script type="text/javascript" src="js/modal.js"></script>