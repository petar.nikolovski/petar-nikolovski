
<div class="container">
        <div class="row text-center">
            <div class="col-md-6 col-sm-6 text-center">
                <div class="article">
                    <div class="article-background"></div>
                    <div class="article-clip"></div>
                    <div class="article-small-img">
                        <img src="../../css/images/photo-cat.jpg" style="" alt="">
                    </div>
                    <div class="article-content">
                        <p class="h3 bold">Dog Adoption Articles</p>
                        <p>Learn more about caring for your new dog.</p>
                    </div>
                    <div class="article-footer">
                        <p class="h5 bold">READ MORE</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 text-center">
                    <div class="article">
                        <div class="article-background"></div>
                        <div class="article-clip"></div>
                        <div class="article-small-img">
                            <img src="../../css/images/photo-cat.jpg" style="" alt="">
                        </div>
                        <div class="article-content">
                            <p class="h3 bold">Dog Adoption Articles</p>
                            <p>Learn more about caring for your new dog.</p>
                        </div>
                        <div class="article-footer">
                            <p class="h5 bold">READ MORE</p>
                        </div>
                    </div>
                </div>
        </div>
    </div>