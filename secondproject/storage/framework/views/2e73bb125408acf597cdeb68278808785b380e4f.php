<div class="modal  bs-example-modal-lg" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg modal-margin">
        <div class="modal-content">
        <form role="form" action='<?php echo e(route('register')); ?>' method="POST">
                <div class="modal-body">
                    <button type="button" class="close marginRight-5px" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <div class="row">
                        <div class="col-md-12" style="margin-bottom:5px">
                            <p class="h3 modal-title" id="myLargeModalLabel">Register</p>
                            
                            <?php if($errors->has('firstname')): ?>
                                <p style="color:red"><?php echo e($errors->first('firstname')); ?></p>
                            <?php endif; ?>
                            <?php if($errors->has('lastname')): ?>
                                <p style="color:red"><?php echo e($errors->first('lastname')); ?></p>
                            <?php endif; ?>
                            <?php if($errors->has('postalcode')): ?>
                                <p style="color:red"><?php echo e($errors->first('postalcode')); ?></p>
                            <?php endif; ?>
                            <?php if($errors->has('emailRegister')): ?>
                                <p style="color:red"><?php echo e($errors->first('emailRegister')); ?></p>
                            <?php endif; ?>
                            <?php if($errors->has('registrationPassword')): ?>
                                <p style="color:red"><?php echo e($errors->first('registrationPassword')); ?></p>
                            <?php endif; ?>
                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" name="firstname" class="form-control" placeholder="Name" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" name="lastname" class="form-control" placeholder="Last Name"
                                    required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <select name="country" class="form-control" required>
                                    <?php $__currentLoopData = $country; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($item->id); ?>"><?php echo e($item->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" name="postalcode" class="form-control" placeholder="Postal Code"
                                    required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="email" name="email" class="form-control" placeholder="Email"
                                    required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4">
                                    <p class="font-size12px" style="margin:0;">You will use
                                        your email to log in</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="password" name="registrationPassword" class="form-control"
                                    placeholder="Password" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="password" name="confirmPassword" class="form-control"
                                    placeholder="Confirm Password" required>
                            </div>
                        </div>
                    </div>
                    <?php echo e(csrf_field()); ?>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group padding-bottom15px" style="padding:8px">
                                <input type="submit" name="register" class="btn btn-block btn-custom"
                                    value="REGISTER NOW">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
