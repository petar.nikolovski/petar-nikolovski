<div class="pets-adoption">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1 class="purplecolor header">Pets Available For Adoption</h1>
            </div>
        </div>
        <div class="row cards-row">
            <div class="col-md-1"></div>
            <?php $__currentLoopData = $random; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pet): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-lg-2 col-sm-4">
                <div class="grid">
                    <a href="/meet/<?php echo e($pet->id); ?>" >
                    <figure class="effect-zoe">
                        <div class="animal-image">
                            <img src="<?php echo e($pet->image); ?>">
                        </div>
                        <div class="panel-clip"></div>
                        <div class="animal-name purplecolor">
                            <p><?php echo e($pet->name); ?></p>
                        </div>
                        <figcaption class="text-center figcaption">
                            <div class="image-on-hover">
                                <img src="<?php echo e($pet->image); ?>" class="circle-image" alt="">
                            </div>
                            <div class="text-on-hover">
                                <p class="h4 purplecolor"><?php echo e($pet->name); ?></p>
                            <p><?php echo e($pet->short_description); ?></p>
                            </div>
                            <div class="social" style="display:none;">
                                <i class="fab fa-facebook-f"></i>
                                <i class="fab fa-twitter"></i>
                                <i class="fab fa-pinterest-p"></i>
                                <i class="far fa-envelope"></i>
                                <i class="far fa-copy"></i>
                                <p class="social-text"><b>Share this pet.</b></p>
                            </div>
                            <div class="hover-button" >
                                <div class="button share"><i id="share" class="fa fa-share purplecolor"></i></div>
                                <div class="button exit"><i id="close" class="fas fa-times" style="display: none;"></i></div>
                            </div>
                        </figcaption>
                    </figure>
                    </a>
            </div>
        </div>
            
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <div class="col-md-2 text-center">
            <div class="card">
                <div class="card-body">
                    <i class="fa fa-paw white" aria-hidden="true"></i>
                    <p>232323 more pets available on PetFinder</p>
                </div>
                <div class="card-footer">
                    <p class="white"><a href="/find/pet/filter">MEET THEM</a></p>
                </div>
            </div>
        </div>
    </div>
    </div>

